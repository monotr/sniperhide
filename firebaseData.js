/*
playerDB
	id  >> listener
		nickname
		experience
		level
		currency
		skins
			skin1
			skin2
			...
		kills
		deaths
		headshots
		longest shot
		time played
		games played
		damage dealt
		average kills
		average damage
		aquaricy
		most survived time
playersOnmatch >> listener
	id
		nickname
		kills
		skin
hourInGame >> listener
*/

var listeningFirebaseRefs = [];

createFirebase = function(playerToken) {
	// Initialize Firebase
	  var config = {
	    apiKey: "AIzaSyBMa04Llgfg68SbfKo9uqbVjrPfrqhBJPQ",
	    authDomain: "taprace-aac8d.firebaseapp.com",
	    databaseURL: "https://taprace-aac8d.firebaseio.com",
	    storageBucket: "taprace-aac8d.appspot.com",
	    messagingSenderId: "892593893204"
	  };
	firebase.initializeApp(config);

	createListeners();
	
	initiateMatchRoutine();

	//hourInGame
	dbHourInGame.on('child_added', function(data) {
		SendMessage ('FirebaseObj', 'setTimeOfDay', data.val());
	});

	//send DONE init firebase
	SendMessage ('FirebaseObj', 'doneInit');
	
}

createListeners = function(){
	//listener a current match-------------------------------------------------------------------- 0
	const dbCurrentMatch = firebase.database().ref().child('playersOnmatch');
	listeningFirebaseRefs.push(dbCurrentMatch);

	//hora del juego-------------------------------------------------------------------- 1
	const dbHourInGame = firebase.database().ref().child('hourInGame');
	listeningFirebaseRefs.push(dbHourInGame);
}

initiateMatchRoutine = function(){
//---MATCH---
	//get current players
	dbCurrentMatch.orderByChild("kills").on('child_added', function(data) {
		SendMessage ('FirebaseObj', 'createScore', data.key, data.nickname, data.skin, data.kills);
	});

	dbCurrentMatch.orderByChild("kills").on('child_changed', function(data) {
		SendMessage ('FirebaseObj', 'updateRankingObj', data.key, data.nickname, data.skin, data.kills);
	});

	dbCurrentMatch.orderByChild("kills").on('child_changed', function(data) {
		
		//data.val().nickname;
		//data.val().kills;
	}
}

initPlayer = function(playerToken, nick, skin, isGuest) {
	//sube datos de jugador a firebase
	var update = {};
	update[playerToken+"/nickname"] = nick;
	update[playerToken+"/skin"] = skin;
	update[playerToken+"/kills"] = 0;
	listeningFirebaseRefs[0].set(update);

	//crea listener de GUEST
	if(!isGuest){ // si no es invitado, accede a su perfil -------------------------------------------------------------------- 2
		const dbMyProfile = firebase.database().ref().child('playerDB/' + playerToken);
		listeningFirebaseRefs.push(dbMyProfile);
	}

	SendMessage ('FirebaseObj', 'createScore', playerToken, nick, skin, 0);

}