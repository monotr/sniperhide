﻿// LoadingScreenManager
// --------------------------------
// built by Martin Nerurkar (http://www.martin.nerurkar.de)
// for Nowhere Prophet (http://www.noprophet.com)
//
// Licensed under GNU General Public License v3.0
// http://www.gnu.org/licenses/gpl-3.0.txt

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScreenManager : MonoBehaviour {

	[Header("Loading Visuals")]
	public Image loadingIcon;
	public Image loadingDoneIcon;
	public Text loadingText;
	public Slider progressBar;
	public Image fadeOverlay;
	public Button startBtn;

	[Header("Timing Settings")]
	public float waitOnLoadEnd = 0.25f;
	public float fadeDuration = 0.25f;

	[Header("Loading Settings")]
	public LoadSceneMode loadSceneMode = LoadSceneMode.Single;
	public ThreadPriority loadThreadPriority;

	[Header("Other")]
	// If loading additive, link to the cameras audio listener, to avoid multiple active audio listeners
	//public AudioListener audioListener;

	AsyncOperation operation;
	Scene currentScene;
	[HideInInspector] public bool isPhotonConnected, isLevelLoaded;
	public GameObject loadingPanel;

	public int sceneToLoad = -1;
	// IMPORTANT! This is the build index of your loading scene. You need to change this to match your actual scene index
	static int loadingSceneIndex = 2;

	public void LoadScene(int levelNum) {				
		Application.backgroundLoadingPriority = ThreadPriority.High;
		sceneToLoad = levelNum;
		SceneManager.LoadScene(loadingSceneIndex);
	}

	void Start() {
		if (sceneToLoad < 0)
			return;

		//photon
		PhotonNetwork.ConnectUsingSettings("0.1");
		PhotonNetwork.autoJoinLobby = true;
		print ("Connected!");

		fadeOverlay.gameObject.SetActive(true); // Making sure it's on so that we can crossfade Alpha
		startBtn.interactable = false;
		currentScene = SceneManager.GetActiveScene();
		StartCoroutine(LoadAsync(sceneToLoad));
	}

	private IEnumerator LoadAsync(int levelNum) {
		ShowLoadingVisuals();

		yield return null; 

		//FadeIn();
		StartOperation(levelNum);

		float lastProgress = 0f;

		// operation does not auto-activate scene, so it's stuck at 0.9
		while (DoneLoading() == false) {
			yield return null;

			if (Mathf.Approximately(operation.progress, lastProgress) == false) {
				progressBar.value = operation.progress;
				lastProgress = operation.progress;
			}
		}

		//if (loadSceneMode == LoadSceneMode.Additive)
			//audioListener.enabled = false;

		ShowCompletionVisuals();

		yield return new WaitForSeconds(waitOnLoadEnd);

		//FadeOut();

		//yield return new WaitForSeconds(fadeDuration);
	}

	public void startGame(){
		if (loadSceneMode == LoadSceneMode.Additive)
			SceneManager.UnloadScene(currentScene.name);
		else
			operation.allowSceneActivation = true;
	}

	private void StartOperation(int levelNum) {
		Application.backgroundLoadingPriority = loadThreadPriority;
		operation = SceneManager.LoadSceneAsync(levelNum, loadSceneMode);


		if (loadSceneMode == LoadSceneMode.Single)
			operation.allowSceneActivation = false;
	}

	private bool DoneLoading() {
		return (loadSceneMode == LoadSceneMode.Additive && operation.isDone) || (loadSceneMode == LoadSceneMode.Single && operation.progress >= 0.9f); 
	}

	void FadeIn() {
		fadeOverlay.CrossFadeAlpha(0, fadeDuration, true);
	}

	void FadeOut() {
		fadeOverlay.CrossFadeAlpha(1, fadeDuration, true);
	}

	void ShowLoadingVisuals() {
		loadingIcon.gameObject.SetActive(true);
		loadingDoneIcon.gameObject.SetActive(false);

		progressBar.value = 0f;
		loadingText.text = "LOADING...";
	}

	void ShowCompletionVisuals() {
		loadingIcon.gameObject.SetActive(false);
		loadingDoneIcon.gameObject.SetActive(true);

		progressBar.value = 1f;
		loadingText.text = "LOADING DONE";

		isLevelLoaded = true;
		if(isPhotonConnected){
			startBtn.interactable = true;
			loadingPanel.SetActive (false);
		}
		else
			loadingText.text = "CONNECTING...";
	}

	//photon connect
	void OnJoinedLobby() {
		RoomOptions roomOptions = new RoomOptions () {IsVisible=false, MaxPlayers=20};
		PhotonNetwork.JoinOrCreateRoom("uniqueRoom", roomOptions, TypedLobby.Default);
		print ("joined looy, joining room...");
	}

	/*void OnPhotonRandomJoinFailed() {
		print("Can't join random room!, creating one");
		PhotonNetwork.CreateRoom(null);
	}*/

	void OnJoinedRoom() {
		print ("Joined room!!!");
		isPhotonConnected = true;
		loadingText.text = "CONNECTED!!!";
		if (isLevelLoaded) {
			startBtn.interactable = true;
			loadingPanel.SetActive (false);
		}
		else
			loadingText.text = "LOADING...";
	}
}