﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class OnlineScript : MonoBehaviour {
	void Start () {
		//creates player
		GameObject newPlayer = PhotonNetwork.Instantiate("playerPrefab", new Vector3(11,20,10), Quaternion.identity, 0) as GameObject;
		RigidbodyFirstPersonController fpController = newPlayer.GetComponent<RigidbodyFirstPersonController> ();
		fpController.isControllable = true;
		fpController.mCamera.SetActive (true);
		PlayerScript playerScript = newPlayer.GetComponentInChildren<PlayerScript> ();
		playerScript.initialSetup ();
	}
}
