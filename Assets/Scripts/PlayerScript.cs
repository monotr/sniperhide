﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AC.TimeOfDaySystemFree;

public class PlayerScript : MonoBehaviour {
	[Header("Sniper")]
	private float curFoV = 60;
	private int FoVstate = 0;
	private Text zoomTxt;
	private GameObject crosshairImg;
	public float bulletSpeed;
	public GameObject[] shotParticles;
	[Header("Sniper recoil")]
	private bool isRecoiling = false;
	private float recoil = 0.0f;
	public float maxRecoil_x = -50f;
	public float maxRecoil_y = 40f;
	public float recoilSpeed = 2f;
	[Header("Camera zoom")]
	public Camera mCam;
	public float minFoV = 1;
	public float maxFoV = 60;
	[Header("Aim")]
	private float t;
	private Vector2 randomPoint;
	private Vector3 lookAtPoint;
	private Quaternion aimRotation;
	public float RADIUS = 0.1f;
	public float MOVEMENT_TIME = 1.0f;
	private bool isAiming = false;
	[Header("Skins")]
	public GameObject[] skinsObj;
	private int curSkin;
	[Header("Stats")]
	public float playerHP;
	public Text playerHPTxt;
	bool isInmune = true;
	public float bulletDamage, sniperRange, refreshTime;
	bool isFresh = true;
	[Header("Meele")]
	public Collider knifeObj;
	private Animation knifeAnim;
	public float knifeRefreshTime;
	bool isKnifeCool = true;
	private Rigidbody myRB;
	[Header("FX")]
	private AudioSource myAudioSource;
	public AudioClip[] playerSounds;
	public float sniperSoundDistance;
	public GameObject soundEmitterObj;
	public GameObject[] particlesObjs;
	[Header("Time")]
	[HideInInspector] public TimeOfDayManager timeManager;
	private GameObject firefliesParticle;
	[Header("Photon")]
	[HideInInspector] public bool isControllable = false;
	[HideInInspector] public PhotonView myPhotonView;
	[HideInInspector] public int myPhotonId;
	[Header("Firebase")]
	private FirebaseStuffScript firebaseScript;

	void Start () {
		myPhotonView = GetComponent<PhotonView> ();
		//firebaseScript = GameObject.Find ("FirebaseObj").GetComponent<FirebaseStuffScript>();
		//gameplay
		knifeAnim = GetComponentInChildren<Animation>();
		knifeObj.enabled = false;

		//----REMOVE WHEN ONLINE!!!!!!!!!!!!!!!!!!!!!!!!
		//initialSetup ();
	}

	public void initialSetup(){
		//settings
		mCam = Camera.main;
		myRB = GetComponent<Rigidbody> ();
		knifeObj.enabled = false;
		//set timeOfDay
		timeManager = GameObject.Find ("Time Of Day Manager").GetComponent<TimeOfDayManager> ();
		//timeManager.dayInSeconds = firebaseScript.timeOfDay;
		//particles
		firefliesParticle = GameObject.Find ("firefliesParticleSystem");
		GetComponentInChildren<KnifeScript> ().playerScript = this;
		GameObject.Find ("RankingPanel").transform.SetParent(GameObject.Find ("Canvas").transform);
		//HUD
		zoomTxt = GameObject.Find ("scopeTxt").GetComponent<Text> ();
		crosshairImg = GameObject.Find ("crosshair");
		crosshairImg.SetActive (false);
		playerHPTxt = GameObject.Find ("HPtxt").GetComponent<Text>();
		playerHPTxt.text = "HP: " + playerHP;
		//audios
		myAudioSource = GetComponent<AudioSource> ();
		//photon
		myPhotonView = GetComponent<PhotonView> ();
		myPhotonId = PhotonNetwork.player.ID;
		//skin
		curSkin = Random.Range (0, skinsObj.Length);
		//skinsObj [curSkin].SetActive (true);

		myPhotonView.RPC("sendCharacterId", PhotonTargets.OthersBuffered, myPhotonId, curSkin);

		//start control
		isInmune = false;
		isControllable = true;
	}

	void Update () {
		if(isControllable){
			//camera zoom
			if (Input.mouseScrollDelta.y != 0) {
				curFoV += Mathf.Sign (Input.mouseScrollDelta.y) * 10;
				if (curFoV > maxFoV)
					curFoV = maxFoV;
				else if (curFoV < minFoV)
					curFoV = minFoV;
				else
					FoVstate += (int)Mathf.Sign (Input.mouseScrollDelta.y);
				mCam.fieldOfView = curFoV;
			}

			if (FoVstate == 0) {
				zoomTxt.text = "";
				crosshairImg.SetActive (false);
				isAiming = false;
			} else {
				zoomTxt.text = "x" + FoVstate;
				crosshairImg.SetActive (true);
				isAiming = true;
			}
				
			//aiming ();
			// END camera zoom

			//sniper recoil
			if(isRecoiling)
				recoiling ();

			//show fireflies at night
			if (timeManager.IsNight && !firefliesParticle.activeSelf) {
				firefliesParticle.SetActive (true);
				timeManager.dayInSeconds /= 2;
			} else if (timeManager.IsDay && firefliesParticle.activeSelf) {
				firefliesParticle.SetActive (false);
				timeManager.dayInSeconds *= 2;
			}
		}
	}

	void FixedUpdate()
	{
		if(isControllable){
			//shoot bullet
			if (Input.GetMouseButtonDown (0) && FoVstate < 0 && isFresh) {
				//recoil
				recoil = 0.20f;
				maxRecoil_x = -Random.Range (25.0f, 51.0f);
				maxRecoil_y = Random.Range (20.0f, 40.0f);
				isRecoiling = true;
				//unfresh
				isFresh = false;
				Invoke ("refreshSniper", refreshTime);
				//play sound and flash effect
				Vector3 muzzlePos = mCam.transform.position + mCam.transform.forward;
				myPhotonView.RPC("spawnParticle_Sound", PhotonTargets.All, muzzlePos, 0, sniperSoundDistance, 0, muzzlePos, Quaternion.identity);
				//check if hit
				RaycastHit hit;
				GameObject newPart_;
				if (Physics.Raycast (muzzlePos, mCam.transform.forward, out hit, sniperRange)) {
					print ("hit at " + hit.distance + " with " + hit.transform.name);
					if (hit.transform.tag == "Player") {
						myPhotonView.RPC("spawnParticle_Sound", PhotonTargets.All, hit.point, 3, sniperSoundDistance/3, 3, hit.point, Quaternion.LookRotation(hit.normal));
						if (hit.collider.GetType () == typeof(SphereCollider)) { //cabeza
							hit.collider.gameObject.GetComponentInParent<PlayerScript> ().sendToAll (0, 100, 0);
						} else if (hit.collider.GetType () == typeof(CapsuleCollider)) { //cuerpo
							hit.collider.gameObject.GetComponentInParent<PlayerScript> ().sendToAll (0, 50, 50);
						}
					} else if (hit.transform.name.Contains ("tree")) {
						myPhotonView.RPC("spawnParticle_Sound", PhotonTargets.All, hit.point, 1, sniperSoundDistance/3, 1, hit.point, Quaternion.LookRotation(hit.normal));
					} else if (hit.transform.name.Contains ("Terrain")) {
						myPhotonView.RPC("spawnParticle_Sound", PhotonTargets.All, hit.point, 2, sniperSoundDistance/3, 2, hit.point, Quaternion.LookRotation(hit.normal));
					}
				}
			}
			// END shoot bullet

			//meele
			if (Input.GetMouseButtonDown (1) && FoVstate == 0 && isKnifeCool) {
				isKnifeCool = false;
				Invoke ("refreshKnife", knifeRefreshTime);
				myPhotonView.RPC("playAnim", PhotonTargets.All, 0);
			}
		}
	}

	//spawn particle and sound
	[PunRPC]
	public void spawnParticle_Sound(Vector3 posS, int audioId, float maxDis, int partObj, Vector3 posP, Quaternion rot){
		//sound
		GameObject newSound = Instantiate(soundEmitterObj, posS, Quaternion.identity) as GameObject;
		AudioSource newSource = newSound.GetComponent<AudioSource> ();
		newSource.clip = playerSounds[audioId];
		newSource.maxDistance = maxDis;
		newSource.Play ();
		Destroy (newSound, playerSounds[audioId].length);

		//particle
		Instantiate (particlesObjs[partObj], posP, rot);
	}

	void recoiling () {
		if (recoil > 0f) {
			Quaternion maxRecoil = Quaternion.Euler (-maxRecoil_x, maxRecoil_y, 0f);
			//Quaternion maxRecoil2 = Quaternion.Euler (0, maxRecoil_y, 0f);
			// Dampen towards the target rotation
			transform.localRotation = Quaternion.Slerp (transform.localRotation, maxRecoil, Time.deltaTime * recoilSpeed/6);
			//player.transform.localRotation = Quaternion.Slerp (transform.localRotation, maxRecoil, Time.deltaTime * recoilSpeed);
			recoil -= Time.deltaTime;
		} else{
			isRecoiling = false;
			recoil = 0f;
			// Dampen towards the target rotation
			transform.localRotation = Quaternion.Slerp (transform.localRotation, Quaternion.identity, Time.deltaTime * recoilSpeed / 6);
			//player.transform.localRotation = Quaternion.Slerp (transform.localRotation, Quaternion.identity, Time.deltaTime * recoilSpeed / 2);
		}
	}

	void aiming () {
		if (isAiming) {
			if (t >= MOVEMENT_TIME) {
				randomPoint = Random.insideUnitCircle * RADIUS;
				//lookFromPoint = lookAtPoint;
				lookAtPoint = new Vector3 (mCam.transform.parent.forward.x + randomPoint.x, mCam.transform.parent.forward.y + randomPoint.y, mCam.transform.parent.forward.z);
				aimRotation = Quaternion.LookRotation (lookAtPoint);
				t = 0.0f;
			} else {
				t += Time.deltaTime;
				//Vector3 relativePos = lookAtPoint - mCam.transform.parent.position;
				mCam.transform.parent.rotation = Quaternion.Slerp (mCam.transform.localRotation, aimRotation, t);
			}
		} else {
			mCam.transform.parent.rotation = Quaternion.identity;
		}
	}

	void refreshSniper(){
		isFresh = true;
	}

	void refreshKnife(){
		isKnifeCool = true;
	}

	void deactivateKnife(){
		knifeObj.enabled = false;
	}

	//knife anim
	[PunRPC]
	public void playAnim(int type){
		switch (type) {
		case 0:
			knifeObj.enabled = true;
			Invoke ("deactivateKnife", 0.5f);
			knifeAnim.Play ();
			break;
		}
	}

	//others players change skin
	[PunRPC]
	public void sendCharacterId(int playerID, int charId)
	{
		knifeAnim = GetComponentInChildren<Animation>();
		knifeObj.enabled = false;
		knifeObj.gameObject.transform.SetParent (transform.parent);
		myPhotonId = playerID;
		curSkin = charId;
		//skinsObj [curSkin].SetActive (true);
	}

	//function that calls changes to all players
	public void sendToAll(int type, float par1, float par2){
		switch (0) {
		case 0://shot
			myPhotonView.RPC("getDamage", PhotonTargets.All, myPhotonId, par1, par2);
			break;
		}
	}

	//receive damage and update all
	[PunRPC]
	public void getDamage(int playerID, float damage, float force)
	{
		if (isControllable && !isInmune) {
			playerHP -= damage;

			if (playerHP > 0) {
				playerHPTxt.text = "HP: " + playerHP;
				myRB.AddForce (-transform.forward * force * 10.0f);
			} else {
				float x = Random.Range (-10, 11);
				float z = Random.Range (-10, 11);
				transform.position = new Vector3 (x, 15, z);

				playerHP = 100;
				playerHPTxt.text = "HP: " + playerHP;
			}
		}
	}
}
