﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class FirebaseStuffScript : MonoBehaviour {
	[HideInInspector] public bool isGuest = true;
	private string playerToken = "testiTest";
	private string playerNickname;
	private int skinId;

	//time of day
	public float timeOfDay;

	LoadingScreenManager loadingScript;

	//UI
	public GameObject scoreObj;
	private Dictionary<string, RankingObj> rankingObj;
	public GameObject rankingPanel;
	public Sprite[] skinsSprites;

	//RankingObj
	public class RankingObj {
		public GameObject rankingObj;
		public Text scoreTxt;
		public Image skinImg;

		public RankingObj(GameObject obj){
			rankingObj = obj;
			scoreTxt = rankingObj.GetComponentInChildren<Text>();
			skinImg = rankingObj.GetComponentInChildren<Image>();
		}
	}

	void Awake() {
		DontDestroyOnLoad(transform.gameObject);

		loadingScript = GameObject.Find ("Settings").GetComponent<LoadingScreenManager>();
		rankingObj = new Dictionary<string, RankingObj> ();

		createFirebase ();
	}

	void createFirebase(){
		Application.ExternalCall("createFirebase", isGuest, playerToken);
	}

	public void doneInit(){
		if (loadingScript.isPhotonConnected)
			Application.ExternalCall ("initPlayer", playerToken, playerNickname, skinId);
		else
			Invoke ("doneInit", 1.0f);
	}

	public void createScore(string firebaseID, string name, int skin, int kills){
		GameObject newScore = Instantiate (scoreObj, Vector3.zero, Quaternion.identity) as GameObject;
		newScore.transform.SetParent (rankingPanel.transform);
		rankingObj.Add (firebaseID, new RankingObj(newScore));
		updateRankingObj (firebaseID, name, skin, kills);
	}

	public void updateRankingObj(string firebaseID, string name, int skin, int kills){
		RankingObj rankObj;
		if (rankingObj.TryGetValue (firebaseID, out rankObj)) {
			rankObj.scoreTxt.text = " " + name + ">" + kills;
			rankObj.skinImg.sprite = skinsSprites[skin];
		}
	}

	public void setTimeOfDay(float timeOD){
		timeOfDay = timeOD;
	}

	void FacebookLogIn () {
		Application.ExternalCall("MyFunction3", "one", 2, 3.0F);
	}
}
