﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeScript : MonoBehaviour {
	public PlayerScript playerScript = null;

	void OnTriggerEnter(Collider other) {
		if (playerScript) { // si ya se asignó script
			if (playerScript.isControllable) { // si es jugador que controlar a personaje
				if (other.tag == "Knife") { // si entrante es tag Knife
					if (other.name.Contains ("head")) { // si entró a cabeza
						other.gameObject.GetComponentInParent<PlayerScript> ().sendToAll (0, 33, 20);
					} else if (other.name.Contains ("torso")) { // si entro a torso
						other.gameObject.GetComponentInParent<PlayerScript> ().sendToAll (0, 25, 20);
					}
				}
			}
		}
	}
}
